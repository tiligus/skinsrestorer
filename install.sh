#!/bin/bash
skinrestorer_wgui=/home/ubuntu/.scripts/plugins/pl-skinsrestorer/remove.sh
node=/home/ubuntu/.nvm/versions/node/v14.15.5/bin/node

if test -f "$skinrestorer_wgui"; then
    echo "SkinRestorer already installed." && exit
fi

sudo mkdir /home/ubuntu/.scripts/plugins && sudo chmod 777 /home/ubuntu/.scripts/plugins
cp content/SkinsRestorer.jar /home/ubuntu/mc-server/plugins/SkinsRestorer.jar
mkdir /home/ubuntu/.scripts/plugins/pl-skinsrestorer/
mv www/ /home/ubuntu/.scripts/plugins/pl-skinsrestorer/www/
mv nodeapp/ /home/ubuntu/.scripts/plugins/pl-skinsrestorer/nodeapp/
mv content/remove.sh $skinrestorer_wgui

curl -H "Content-type: application/json" -d '{"add_record":true, "name":"SkinsRestorer 14.1.13","remove_script":"/home/ubuntu/.scripts/plugins/pl-skinsrestorer/remove.sh","file_0":"SkinsRestorer/messages.yml","file_1":"SkinsRestorer/config.yml","file_2":"SkinsRestorer/command-messages.properties"}' 'http://127.0.0.1:8181' # add plugin to panel

cd /home/ubuntu/.scripts/plugins/pl-skinsrestorer/nodeapp/
$node app.js